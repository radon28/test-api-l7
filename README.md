# Test API on Laravel 7 & passport
Steps to follow for setup of project.
```sh
$ composer install
$ php artisan key:generate
```
Set up database from .env file
```sh
$ php artisan migrate
$ php artisan passport:install
```