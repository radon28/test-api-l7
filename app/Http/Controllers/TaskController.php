<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Task;
use App\Http\Requests\TaskReq;

class TaskController extends Controller
{
    public function create(TaskReq $req)
    {
        $validated = $req->validated();
        $task=Task::create([
          "body"=>$req->body,
          'created_by'=>auth()->user()->id
        ]);

        return response()->json(['message'=>"Task created", 'data'=>$task], 201);
    }

    public function list()
    {
        $tasks=Task::where('created_by', auth()->user()->id)->get();
        return response()->json(['message'=>"User Task List", 'data'=>$tasks], 200);
    }

    public function getTask(Task $task)
    {
        $this->authorize('view', $task);
        return response()->json(['message'=>"User Task", 'data'=>$task], 200);
    }

    public function update(Task $task, TaskReq $req)
    {
        $this->authorize('update', $task);
        $validated = $req->validated();
        $task->body=$req->body;
        $task->save();
        return response()->json(['message'=>"User Task updated", 'data'=>$task], 200);
    }

    public function delete(Task $task)
    {
        $this->authorize('delete', $task);
        $task->delete();
        return response()->json(['message'=>"User Task deleted", 'data'=>null], 200);
    }
}
