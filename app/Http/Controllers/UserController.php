<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests\UserCreate;
use App\Http\Requests\Login;
use App\User;
use Carbon\Carbon;

class UserController extends Controller
{
    public function login(Login $request)
    {
      $validated = $request->validated();
      $credentials = request(['email', 'password']);
      if (auth()->attempt($credentials)) {
          $token = auth()->user()->createToken('Personal Access Token');
          return response()->json([
              'access_token' => $token->accessToken,
              'token_type' => 'Bearer',
              'expires_at' => Carbon::parse($token->token->expires_at)->toDateTimeString()
          ], 200);
      } else {
          return response()->json(['message'=>"Invalid Email or password", 'error' => 'Unauthorised'], 401);
      }
    }

    public function register(UserCreate $req)
    {
        $validated = $req->validated();
        $user=User::create([
            'name'=>trim($req->name),
            'email'=>strtolower(trim($req->email)),
            'password'=>bcrypt($req->password)
        ]);
        return response()->json(['message'=>"User created", 'data'=>['name'=>$user->name, 'email'=>$user->email]], 201);
    }
}
