<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        if($exception instanceof \Illuminate\Auth\AuthenticationException) {
            return response()->json(['message'=>'Unauthenticated', 'type'=>'auth','error'=>$exception->getMessage()], 401);
        } if($exception instanceof \Illuminate\Auth\Access\AuthorizationException){
            return response()->json(['message'=>'Unauthorized', 'type'=>'auth','error'=>$exception->getMessage()], 403);
        } elseif($exception instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException) {
            return response()->json(['message'=>'Url not found', 'type'=>'http', 'error'=>"Url not found"], 404);
        } elseif ($exception instanceof \Illuminate\Database\Eloquent\ModelNotFoundException) {
            return response()->json(['message'=>'Entity not found', 'type'=>'entity', 'error'=>$exception->getMessage()], 404);
        } elseif ($exception instanceof \Spatie\Permission\Exceptions\UnauthorizedException) {
            return response()->json(['message'=>'Unauthorized', 'type'=>'auth-role', 'error'=>$exception->getMessage()], 403);
        }
        return response()->json(['message'=>'Internal Server Error', 'type'=>'server','error'=>$exception->getMessage()], 500);
    }
}
